#!/usr/bin/python

import Queue
import time
import threading
import base_method as bs
#import espeak
import os
import subprocess

queueLock = threading.Lock()

class myEspeak:
    def __init__(self):
        pass
    def say(self, text):
        command = 'espeak "' + text + '" 120 -ven {} --stdout | aplay'
        #subprocess.call('espeak "{line}" 120 -ven {} --stdout | aplay'.format(line=text), shell=True)
        subprocess.call(command, shell=True)

class ESpeakHandler:
    def __init__(self):
        #es = espeak.ESpeak(options)
        self.es = myEspeak()
        
        self.buffer = Queue()
        self.t = espeakThread(self.buffer, self.es)
        
    def start(self):
        self.t.start()
            
    def pause(self):
        self.t._pause_()
    
    def resume(self):
        self.t._resume_()
        
    def kill(self):
        self.t._kill_()



class espeakThread (threading.Thread):
    def __init__(self, q, es):
        threading.Thread.__init__(self)
        self.q = q
        self.es = es
        self.exitFlag = False
        self.pauseFlag = False
        
    def run(self):
        self.exitFlag = False
        self.pauseFlag = False
        print "[ %s ] Starting thread" %(bs.getCurrentDateTime())
        self.process_data()
        print "[ %s ] Closing thread " %(bs.getCurrentDateTime())
        
    def _pause_(self):
        self.pauseFlag = True

    def _resume_(self):
        self.pauseFlag = False
        
    def _kill_(self):
        self.exitFlag = True
        
    def _setStart_(self):
        self.exitFlag = False

    def process_data(self):
        blank_counter = 0
        while not self.exitFlag:
            if not self.pauseFlag:
                if not self.q.isEmpty():
                    data = str(self.q.dequeue())
                    print "[ %s ] tts >> %s" %(bs.getCurrentDateTime(), data)
                    self.es.say(data)
                    blank_counter = 0
                else:
                    # queue is empty reload it
                    time.sleep(0.5)
                    blank_counter += 1
                    if blank_counter > 4:
                        blank_counter = 0
                        temp_menu = self.q.menu
                        self.q.menu = [["message", "Reapeating menu"]] + temp_menu
                        self.q.fillMenu()
                        self.q.menu = temp_menu
            else:
                #thread is paused
                time.sleep(0.01)   #1

class Queue:
    def __init__(self):
        self.items = []
        self.menu = [["message","Welcome to Notice Board"]]
        
    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        queueLock.acquire()
        self.items.insert(0,item)
        queueLock.release()
        
    def dequeue(self):
        queueLock.acquire()
        item = self.items.pop()
        queueLock.release()
        return item

    def size(self):
        return len(self.items)
        
    def flushAll(self):
        queueLock.acquire()
        while not self.isEmpty():
            self.items.pop()
        queueLock.release()
    
    def setMenu(self, menu):
        # storing current Menu
        self.menu = menu
        
    def fillMenu(self):
        # Filling the queue with menu
        print "[ %s ] Clearing queue. " %(bs.getCurrentDateTime())
        self.flushAll()
        print "[ %s ] [start] Filling queue with menu" %(bs.getCurrentDateTime())
        for item in self.menu:
            line = None
            if "message" in str(item[0]).lower():
                line = str(item[1])
            else:
                if len(item) == 4:
                    if "folder" in str(item[2]).lower():
                        line = "Press " + str(str(item[1])) + " for selecting " + str(str(item[3]))
                    elif "file" in str(item[2]).lower():
                        line = "Press " + str(str(item[1])) + " for listening " + str(str(item[3]))
                    else:
                        line = "Press " + str(str(item[1])) + " for " + str(str(item[3]))
                else:
                    print "[ %s ] Wrong menu item %s" %(bs.getCurrentDateTime(), item)
            if line:
                print "[ %s ] Adding line: %s" %(bs.getCurrentDateTime(), line)
                self.enqueue(line)
        print "[ %s ] [ end ] Filling queue with menu" %(bs.getCurrentDateTime())
            

    def fillFile(self, fileName):
        # Filling menu with file
        print "[ %s ] Clearing queue. " %(bs.getCurrentDateTime())
        self.flushAll()
        
        print "[ %s ] [start] Filling queue with file: %s" %(bs.getCurrentDateTime(), fileName)
        line = "Reading notice %s, Press any key to stop reading this notice"%(os.path.splitext(os.path.basename(fileName))[0])
        print "[ %s ] Adding line: %s" %(bs.getCurrentDateTime(), line)
        self.enqueue(line)

        with open(fileName) as f:
            for line in f:
                print "[ %s ] Adding line: %s" %(bs.getCurrentDateTime(), line)
                self.enqueue(line)
        print "[ %s ] [ end ] Filling queue with file: %s" %(bs.getCurrentDateTime(), fileName)
        
if __name__ == "__main__":
    esh = ESpeakHandler()
    esh.start()
    
    menu = [
            ["message", "Welcome to Notice board"],
            ["menu", 1, "folder", "Junior College "],
            ["menu", 2, "folder", "Senior College "],
            ["menu", 3, "folder", "Post Graduation "],
            ["menu", 4, "option", "more options"]
            ]
    esh.buffer.setMenu(menu)
    esh.buffer.fillMenu()
    #time.sleep(1)    # 10 
    
    print "[ %s ] Pausing"%(bs.getCurrentDateTime())
    esh.pause()
    #time.sleep(1)    #10
    
    print "[ %s ] Resuming"%(bs.getCurrentDateTime())
    esh.resume()
    #time.sleep(1)    #10
    
    esh.buffer.fillFile("/opt/notice_board/notice_board/program/main.py")
    #time.sleep(1)    #10
    
    print "[ %s ] Pausing"%(bs.getCurrentDateTime())
    esh.pause()
    #time.sleep(1)    #10
    
    print "[ %s ] Resuming"%(bs.getCurrentDateTime())
    esh.resume()
    #time.sleep(1)    #10

    while not esh.buffer.isEmpty():
        time.sleep(0.1)    #5
    time.sleep(50)    #1000
    esh.kill()
