#!/usr/bin/python
import sys
import base_method as bs
import notice_board
import signal
import time
import traceback

def exit_gracefully(signum, frame):
    if nb != None:
        nb.exit()

if __name__ == "__main__":
    print "[ %s ] main loop started " %(bs.getCurrentDateTime())
    
    nb = None
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    
    try:
        nb = notice_board.NoticeBoard()
        errorCode = nb.menuHandler()
    except:
        print "Error in __main__"
        errorCode = 1
        try:
            exc_info = sys.exc_info()
        finally:
            traceback.print_exception(*exc_info)
            del exc_info
    
    if nb != None: 
        nb.exit()
    
    print "[ %s ] main loop ended "%(bs.getCurrentDateTime())
    sys.exit(errorCode)
