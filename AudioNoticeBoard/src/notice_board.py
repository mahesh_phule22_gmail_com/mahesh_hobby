#!/usr/bin/python
import base_method as bs
import espeak_handler
import copy

#default_parent_folder_path = "/opt/notice_board/notice_board/notices"
#default_parent_folder_path = "/opt/notices/"
default_parent_folder_path = "/home/pi/notices/"
default_parent_folder = "notices"

moreOptionsMenu = [
                    ["menu", 1, "option", "to listen previous set"],
                    ["menu", 2, "option", "to listen next set"],
                    ["menu", 3, "option", "to go in previous menu"],
                    ["menu", 4, "option", "to repeate current set"]
                ]
testMenu = [
            ["message", "Welcome to Notice board"],
            ["menu", 1, "folder", "Junior College "],
            ["menu", 2, "folder", "Senior College "],
            ["menu", 3, "folder", "Post Graduation "],
            ["menu", 4, "option", "more options"]
            ]

nextSetStr = "listining next set of options"
prevSetStr = "listining previous set of options"
repeatSetStr = "listining current set again"
prevMenuStr = "going to previous menu"
moreOptionStr = "listing more option"

class NoticeBoard:
    errorCode = 0
    filestructure = None
    
    def __init__(self):
        self.errorCode = 0
        
        print "[", bs.getCurrentDateTime(), "] parent folder:", default_parent_folder_path
        try :
            self.filestructure = bs.getDirectoryStructure(default_parent_folder_path)
        except:
            print "[", bs.getCurrentDateTime(), "] Error in loading file structure"
            self.errorCode = 1
        print "[", bs.getCurrentDateTime(), "] required file structre loaded."
        
        '''print "[", bs.getCurrentDateTime(), "] Loaded file structure:"
        print self.filestructure[default_parent_folder]'''
        
        print "[", bs.getCurrentDateTime(), "] [start] Starting espeak handler."
        self.esh = espeak_handler.ESpeakHandler()
        self.esh.start()
        print "[", bs.getCurrentDateTime(), "] [ end ] Starting espeak handler."
        self.state = "initial"
        self.completeMenuChanged = True
        self.currentMenuChanged = False
        self.setMoreOptionMenu = False
        self.keyList = [default_parent_folder]
        self.currentMenu = []
        self.completeMenu = []
        self.optionMenu = []
        self.setCounter = 1
        self.maxSetCounter = 1
        
        
    def menuHandler(self):
        if self.errorCode != 0:
            return self.errorCode
        while self.state != "kill":
            # generate and set menu
            self.generateAndFillMenu()
            
            # read key wait for key input **blocking call** 
            # what next? decide based on input key
            self.processKey(bs.getKey())
        return self.errorCode

        
    def processKey(self, key):
        self.completeMenuChanged = False
        match_found = False
        
        if key == 9:
            # key 9 is considered to end the program
            print "[", bs.getCurrentDateTime(), "] tts >> Stopping Notice board "
                
            self.esh.pause()
            self.esh.es.say("Stopping Notice board" )
            self.esh.resume()
            
            print "[", bs.getCurrentDateTime(), "] [start] Exit process"
            self.exit()
            print "[", bs.getCurrentDateTime(), "] [ end ] Exit process"
        elif key == 8:
            # key 8 is considered to suspend and re-enable 
            if self.state == "suspend":
                self.exitSoftwareSuspend()
                print "[", bs.getCurrentDateTime(), "] Exited from suspend mode"
            else:
                self.enterSoftwareSuspend()
                print "[", bs.getCurrentDateTime(), "] Entered in suspend mode"
        else:
            print "[", bs.getCurrentDateTime(), "] tts >> ", str(key)
            self.esh.pause()
            self.esh.es.say(str(key))
            self.esh.resume()
            if self.state == "suspend":
                pass
            elif self.state == "reading_file":
                # just flush a buffer, it will be automatically re-loaded with previous menu
                self.esh.buffer.flushAll()
                print "[", bs.getCurrentDateTime(), "] Stopped reading file"
                self.state = "reading_menu"
            else:
                for menu in self.currentMenu:
                    if "menu" in menu[0]:
                        if len(menu) == 4:
                            if str(menu[1]) == str(key):
                                match_found = True
                                if str(menu[2]) == "folder":
                                    self.keyList.append(menu[3])
                                    self.completeMenuChanged = True
                                elif str(menu[2]) == "file":
                                    filename = str(default_parent_folder_path).rstrip('/')
                                    for item in self.keyList:
                                        if item != self.keyList[0]:
                                            filename = filename + '/' + str(item) 
                                    filename = filename + '/' + str(menu[3])
                                    
                                    self.esh.pause()
                                    self.esh.buffer.fillFile(filename)
                                    self.esh.resume()
                                    
                                    self.state = "reading_file"
                                elif str(menu[2]) == "option":
                                    # process according to options
                                    if str(menu[3]) == nextSetStr:
                                        self.setCounter += 1
                                        self.currentMenuChanged = True
                                        self.completeMenuChanged = False
                                        self.setMoreOptionMenu = False
                                    elif str(menu[3]) == prevSetStr:
                                        self.setCounter -= 1
                                        self.currentMenuChanged = True
                                        self.completeMenuChanged = False
                                        self.setMoreOptionMenu = False
                                    elif str(menu[3]) == repeatSetStr:
                                        self.currentMenuChanged = True
                                        self.completeMenuChanged = False
                                        self.setMoreOptionMenu = False
                                    elif str(menu[3]) == prevMenuStr:
                                        if len(self.keyList) > 1:
                                            self.keyList.pop()
                                            self.completeMenuChanged = True
                                            self.setMoreOptionMenu = False
                                        else:
                                            print "[ %s ] Wrong pop request." %(bs.getCurrentDateTime())
                                    elif str(menu[3]) == moreOptionStr:
                                        self.completeMenuChanged = False
                                        self.currentMenuChanged = False
                                        self.setMoreOptionMenu = True
                                    else:
                                        # wrong menu option
                                        print "[ %s ] Wrong option menu item %s" %(bs.getCurrentDateTime(), menu)
                                else:
                                    # wrong menu
                                    print "[ %s ] Wrong menu item %s" %(bs.getCurrentDateTime(), menu)
                        else:
                            print "[", bs.getCurrentDateTime(), "] Invalid menu :", menu 
                if not match_found:
                    print "[", bs.getCurrentDateTime(), "] tts >> Wrong key pressed."
                    self.esh.pause()
                    self.esh.es.say("Wrong key pressed.")
                    self.esh.resume()
        
        
    def generateAndFillMenu(self):
        if self.completeMenuChanged:
            self.completeMenuChanged = False
            self.completeMenu = self.generateCompleteMenu()
            self.currentMenuChanged = True
            self.setCounter = 1
            
        menuChanged = True
        if self.currentMenuChanged:
            self.currentMenuChanged = False
            if len(self.keyList) > 1:
                lenghtOfMenu = 1    #for back option
            else:
                lenghtOfMenu = 0
            lenghtOfMenu += self.getMenuCount(self.completeMenu)
            
            if lenghtOfMenu > 4:
                if (self.setCounter < 1):
                    print "[ %s ] Wrong set counter %s" %(bs.getCurrentDateTime(), self.setCounter)
                    self.setCounter = 1
                if (self.setCounter > self.maxSetCounter):
                    print "[ %s ] Wrong set counter %s" %(bs.getCurrentDateTime(), self.setCounter)
                    self.setCounter = self.maxSetCounter
                # deviding menu in sections of 4 or set previous set or set next set
                index = (self.setCounter-1) * 3
                self.currentMenu = self.completeMenu[index:]
                while self.getMenuCount(self.currentMenu) > 3:
                    self.currentMenu.pop()
                counter = 1
                for menu in self.currentMenu:
                    if menu[0] == "menu":
                        menu[1] = counter
                        counter += 1
                #generate more option menu
                self.generateMoreOptionsMenu()
                self.currentMenu += [["menu", str(counter), "option", moreOptionStr]]
            else:
                self.currentMenu = self.completeMenu[:]
                if len(self.keyList) > 1:
                    counter = self.getMenuCount(self.currentMenu) + 1
                    self.currentMenu += [["menu", str(counter), "option", prevMenuStr]]
        elif self.setMoreOptionMenu:
            self.currentMenu = self.optionMenu[:]    # set more option menu
        else:
            menuChanged = False
            
        if menuChanged:
            self.esh.buffer.setMenu(self.currentMenu)
            self.esh.pause()
            self.esh.buffer.fillMenu()
            self.esh.resume()
            self.state = "reading_menu"

    def getMenuCount(self, menu):
        counter = 0
        for menuItem in menu:
            if str(menuItem[0]).lower() == "menu":
                counter += 1
        return counter
        
        
    def generateMoreOptionsMenu(self):
        counter = 1
        if len(self.keyList) > 1:
            self.optionMenu = [["menu", str(counter), "option", prevMenuStr]]
            counter += 1
        else:
            self.optionMenu = None
            
        if self.setCounter > 1:
            if self.optionMenu:
                self.optionMenu += [["menu", str(counter), "option", prevSetStr]]
                counter += 1
            else:
                self.optionMenu = [["menu", str(counter), "option", prevSetStr]]
                counter += 1
                
        if self.setCounter < self.maxSetCounter:
            if self.optionMenu:
                self.optionMenu += [["menu", str(counter), "option", nextSetStr]]
                counter += 1
            else:
                self.optionMenu = [["menu", str(counter), "option", nextSetStr]]
                counter += 1
                
        if self.optionMenu:
            self.optionMenu  += [["menu", str(counter), "option", repeatSetStr]]
            counter += 1
        else:
            self.optionMenu = [["menu", str(counter), "option", repeatSetStr]]
            counter += 1
                
                
    def generateCompleteMenu(self):
        # generating actual complete menu 
        if self.state == "initial":
            self.keyList = [default_parent_folder]          # Fill with initial menu
            menu = [["message", "Welcome To Notice Board"]]
        else:
            menu = None
        
        currentDict = copy.deepcopy(self.filestructure)
        for key in self.keyList:
            if currentDict[key] == {}:
                mMessage = [["message", "There is no file or folder in folder " + key + ". Moving to previous List."]]
                if menu:
                    menu += mMessage
                else:
                    menu = mMessage
                break
            else: 
                currentDict = currentDict[key]
        
        counter = 1
        for item in currentDict:
            if currentDict[item] is None:
                # there is file to read
                if menu:
                    menu += [["menu", str(counter), "file", item]]
                else:
                    menu = [["menu", str(counter), "file", item]]
            else:
                if menu:
                    menu += [["menu", str(counter), "folder", item]]
                else:
                    menu = [["menu", str(counter), "folder", item]]
            counter += 1
        
        """    
        if menu == None :
            print "\t\t menu is None"
            print "\t\t self.keyList", self.keyList
            print "\t\t currentDict", currentDict
            print "\t\t self.filestructure", self.filestructure
        """    
        # calculate max set counter
        self.maxSetCounter = int(self.getMenuCount(menu) /3) + 1
        
        return menu

    
    def enterSoftwareSuspend(self):
        self.state = "suspend"
        self.esh.pause()
        menu = [["message", "Welcome to Notice board"]]
        self.esh.buffer.setMenu(menu)
        self.esh.buffer.fillMenu()
        
        
    def exitSoftwareSuspend(self):
        self.esh.resume()
        self.state = "initial"
        self.completeMenuChanged = True
        
        
    def exit(self):
        self.esh.kill()
        self.state = "kill"
