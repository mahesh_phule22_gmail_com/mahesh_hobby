#!/usr/bin/python

import datetime
import RPi.GPIO as GPIO
import time

def getCurrentDateTime():
    return str(datetime.datetime.now())

from os import sep, walk

def getDirectoryStructure(rootdir):
    """
    Creates a nested dictionary that represents the folder structure of rootdir
    from site http://code.activestate.com/recipes/577879-create-a-nested-dictionary-from-oswalk/ 
    """
    dir = {}
    rootdir = rootdir.rstrip(sep)
    start = rootdir.rfind(sep) + 1
    for path, dirs, files in walk(rootdir):
        folders = path[start:].split(sep)
        subdir = dict.fromkeys(files)
        parent = reduce(dict.get, folders[:-1], dir)
        parent[folders[-1]] = subdir
    return dir

# board numbering System
GPIO.setmode(GPIO.BOARD)
BTN_1 = 18 # G24
BTN_2 = 15 # G22
BTN_3 = 13 # G27
BTN_4 = 11 # G17
BTN_OFF = 22 # G25

btn2key = {
	BTN_1: 1,
	BTN_2: 2,
	BTN_3: 3,
	BTN_4: 4,
	BTN_OFF: 9,
}

eventFlags = {
	BTN_1: False,
	BTN_2: False,
	BTN_3: False,
	BTN_4: False,
	BTN_OFF: False,
}

def setup():
    for btn in btn2key:
        GPIO.setup(btn, GPIO.IN)
        
def cleanGPIO():
    GPIO.cleanup()  

setup()

def getKey():
    key = None
    try:
        while True:
            for btn in btn2key:
                if (not eventFlags[btn]) and (not GPIO.input(btn)):
                    eventFlags[btn] = True
                if eventFlags[btn] and GPIO.input(btn):
                    eventFlags[btn] = False
                    return btn2key[btn]
            time.sleep(.04)
    except Exception as e:
        print "[", getCurrentDateTime(), "] Error in getting key : ", e
        return 9
    return key
    
"""
def getKey():
    key = None
    try:
        key = int( input("Please Press Key to continue") )
    except Exception as e:
        print "[", getCurrentDateTime(), "] Error in getting key : ", e
        return 9
    return key
"""    