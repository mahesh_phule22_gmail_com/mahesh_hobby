import RPi.GPIO as GPIO
import time
import os
import signal
import subprocess

BTN_ON = 37 # G26
BTN_OFF = 22 # G25
CMD = "python /root/m/src/main.py"

GPIO.setmode(GPIO.BOARD)
GPIO.setup(BTN_ON,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(BTN_OFF,GPIO.IN,pull_up_down=GPIO.PUD_UP)

while True:
    try:
        if GPIO.wait_for_edge(BTN_ON, GPIO.FALLING):
            pro = subprocess.Popen("exec " + CMD, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
            if GPIO.wait_for_edge(BTN_OFF, GPIO.FALLING):
                os.killpg(os.getpgid(pro.pid), signal.SIGTERM)  # Send the signal to all the process groups
    except KeyboardInterrupt:
        GPIO.cleanup()       # clean up GPIO on CTRL+C exit

