import RPi.GPIO as GPIO
import uinput
import time

# board numbering System
GPIO.setmode(GPIO.BOARD)
BTN_1 = 18 # G24
BTN_2 = 15 # G22
BTN_3 = 13 # G27
BTN_4 = 11 # G17
BTN_ON = 37 # G26
BTN_OFF = 22 # G25

"""
# BCM Numbering system
GPIO.setmode(GPIO.BCM)
BTN_1 = 24
BTN_2 = 22
BTN_3 = 27
BTN_4 = 17
BTN_ON = 26
BTN_OFF = 25
"""

btn2key = {
	BTN_1: uinput.KEY_1,
	BTN_2: uinput.KEY_2,
	BTN_3: uinput.KEY_3,
	BTN_4: uinput.KEY_4,
	BTN_ON: uinput.KEY_0,
	BTN_OFF: uinput.KEY_9,
}

uinput_events = [
	uinput.KEY_1,
	uinput.KEY_2,
	uinput.KEY_3,
	uinput.KEY_4,
	uinput.KEY_0,
	uinput.KEY_9,
    uinput.KEY_ENTER,
]

eventFlags = {
	BTN_1: False,
	BTN_2: False,
	BTN_3: False,
	BTN_4: False,
	BTN_ON: False,
	BTN_OFF: False,
}

device = uinput.Device(uinput_events)

def setup():
    # setupGPIO
    #GPIO.setwarnings(False) # because I'm using the pins for other things too!
    for btn in btn2key:
        GPIO.setup(btn, GPIO.IN)
        
def loop():
    for btn in btn2key:
        if (not eventFlags[btn]) and (not GPIO.input(btn)):
            eventFlags[btn] = True
            device.emit(btn2key[btn], 1)
            device.emit(uinput.KEY_ENTER, 1)
        if eventFlags[btn] and GPIO.input(btn):
            eventFlags[btn] = False
            device.emit(btn2key[btn], 0)
            device.emit(uinput.KEY_ENTER, 0)
        
def cleanGPIO():
    GPIO.cleanup()  

def testKeys():
    for btn in btn2key:
        device.emit_click(btn2key[btn])
try:  
    setup()
    while True:
        loop()
        time.sleep(.04)
except KeyboardInterrupt:  
    cleanGPIO()       # clean up GPIO on CTRL+C exit  
cleanGPIO()           # clean up GPIO on normal exit  
