from projectConstants import *

#import ImageGrab
import pyscreenshot as ImageGrab
import os
import time

def screenGrab():
    box = (x_pad+1, y_pad+1, x_pad+640, y_pad+480)
    im = ImageGrab.grab(box)
    im.save(os.getcwd() + '/full_snap__' + str(int(time.time())) +
'.png', 'PNG')

def main():
    screenGrab()

if __name__ == '__main__':
    main()

