from quickGrab import *
from mouseInterface import *
from projectConstants import *
    
def startGame():
    # at starting we are follow specific click pattern
    init_sequence_cordList = [ (Cord.init_sound, "Toggling sound..."), 
                            (Cord.init_play, "clicking on play button..."), 
                            (Cord.init_iphone, "clicking on iphone continue button..."),
                            (Cord.init_tutSkip, "clicking on skip button..."), 
                            (Cord.init_goal, "clicking on today's goal continue button...") ]
    for cord_button in init_sequence_cordList:
        mousePos(cord_button[0])
        print "\n", cord_button[1]
        leftClick()
        time.sleep(.1)
        
def clear_tables():
    for cord_plate in Cord.plate:
        mousePos(cord_plate[0])
        print "\n", cord_plate[1]
        leftClick()
    time.sleep(1)

def makeFood(food):
    if food in recipe:
        print 'Making a ', food
        for ingrad in recipe[food]:
            for num_ingrad in range(recipe[food][ingrad]):
                mousePos(Cord.food_ingrad[ingrad])
                leftClick()
                time.sleep(.05)
        time.sleep(.05)
        foldMat()
        time.sleep(1.5)
    else:
        print 'Recepie not available for ', food

def foldMat():
    mousePos((Cord.food_ingrad['rice'][0]+40, Cord.food_ingrad['rice'][1]))
    print 'folding mat' 
    leftClick()
    time.sleep(.1)

def main():
    startGame()

if __name__ == '__main__':
    main()
