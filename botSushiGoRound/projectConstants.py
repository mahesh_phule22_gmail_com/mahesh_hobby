""" 
All coordinates assume a screen resolution of 1280x1024, and Chrome 
maximized with the Bookmarks Toolbar enabled.
Down key has been hit 4 times to centre play area in browser.
x_pad = 19
y_pad = 309
Play area =  x_pad+1, y_pad+1, 660, 790
"""

x_pad = 19
y_pad = 309

class Cord:
    # this class contains coordinates of various game function points    
    # At initialisation
    init_sound = (340, 370) # mute/un-mute sound 
    init_play = (310, 200) # play button
    init_iphone = (320, 390) # i-phone add continue button
    init_tutSkip = (580, 455) # tutorial skip button
    init_goal = (340, 370) # today's goal continue button

    #food ingredients selection counter 
    food_ingrad = dict()
    food_ingrad['shrimp'] = (37, 335)
    food_ingrad['rice'] = (94, 335)
    food_ingrad['nori'] = (37, 390)
    food_ingrad['roe'] = (94, 390)
    food_ingrad['salmon'] = (37, 445)
    food_ingrad['unagi'] = (94, 445)

    #co-ordinates of the food plates and their click action strings
    plate = [ ((80, 208), "Clearing Plate 1..."), 
                      ((180, 208), "Clearing Plate 2..."), 
                      ((280, 208), "Clearing Plate 3..."), 
                      ((380, 208), "Clearing Plate 4..."), 
                      ((480, 208), "Clearing Plate 5..."), 
                      ((580, 208), "Clearing Plate 6...") ]
    
    phone = (580,360)
    phone_cut = (590,340)
    
    phone_topping = (540, 275)
    phone_rice = (540, 296)
    phone_sake = (540, 320)



recipe = dict()
recipe['onigiri'] = {'rice':2, 'nori':1}
recipe['california_roll'] = {'rice':1, 'nori':1, 'roe':1}
recipe['gunkan_maki'] = {'rice':1, 'nori':1, 'roe':2}

