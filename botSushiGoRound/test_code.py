import code
import os

def main():
    menu()
    choice= int(input("Enter menu choice:\t"))
    while choice != 7:
        if choice == 0:
            code.startGame()
        elif choice == 1:
            code.clear_tables()
        elif choice == 2:
            code.foldMat()
        elif choice == 3:
            code.makeFood('onigiri')
        elif choice == 4:
            code.makeFood('california_roll')
        elif choice == 5:
            code.makeFood('gunkan_maki')
        elif choice == 6:
            code.makeFood('12345')
        menu()
        choice = int(input("Enter menu choice:\t"))


def menu():
    os.system('clear')
    print("Choose a number to continue:\t\n\
    Select 0 to start game\n\
    Select 1 to clear_tables\n\
    Select 2 to foldMat\n\
    Select 3 to make food 'onigiri'\n\
    Select 4 to make food 'california_roll'\n\
    Select 5 to make food 'gunkan_maki'\n\
    Select 6 to make food '12345'\n\
    Select 7 to exit programme\n")
        
if __name__ == '__main__':
    main()
